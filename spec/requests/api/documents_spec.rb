# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::DocumentsController, type: :request do
  describe 'GET /index' do
    context 'when the requester app is authorized' do
      before do
        simulate_app_authorized
      end

      it 'returns http success' do
        simulate_app_authorized
        get '/api/documents/index'
        expect(response).to have_http_status(:success)
      end

      it 'returns content type application/json' do
        get '/api/documents/index'
        expect(response.content_type).to include('application/json')
      end

      it 'success if request is authenticable' do
        get '/api/documents/index'
        expect(response).to have_http_status(:success)
      end
    end

    context 'when the requester app is not authorized' do
      it 'not authorized if request is not authenticable' do
        simulate_app_not_authorized
        get '/api/documents/index'
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
