# frozen_string_literal: true

require 'rocali/commands/authorize_api_request'

module JwtAuthentication
  def simulate_app_authorized
    command = double('command', result: 'app_name', success?: true)
    allow(Rocali::AuthorizeApiRequest).to receive(:call).and_return(command)
  end

  def simulate_app_not_authorized
    command = double('command', result: nil, success?: false, errors: 'token errors')
    allow(Rocali::AuthorizeApiRequest).to receive(:call).and_return(command)
  end
end
