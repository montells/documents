class CustomFailure < Devise::FailureApp
  def redirect_url
    user_doorkeeper_omniauth_authorize_url
  end

  def cookies
    request.cookies
  end

  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end
end