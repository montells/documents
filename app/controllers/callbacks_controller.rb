class CallbacksController < Devise::OmniauthCallbacksController

  def doorkeeper
    user = User.find_or_create_by!(email: request.env['omniauth.auth'].uid)
    sign_in_and_redirect user
  end

  def failure
    redirect_to '/422.html'
  end
end
