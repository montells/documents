class SessionsController < Devise::SessionsController
  def destroy
    cookies.delete '_oauth_server_session'
    reset_session
    redirect_to root_url
  end
end
