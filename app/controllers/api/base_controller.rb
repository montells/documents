# frozen_string_literal: true

require 'rocali/commands/strategies/doorkeeper/authorize_api_request'

module Api
  class BaseController < ActionController::API
    include Rocali

    def authenticate_request
      authorize_api_command = Rocali::Strategies::Doorkeeper::AuthorizeApiRequest.call(request.headers)
      @requester_app = authorize_api_command.result
      render json: {error: "Not authorized. #{authorize_api_command.errors}"}, status: 401 unless authorize_api_command.success?
    end
  end
end
