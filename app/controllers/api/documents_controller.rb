module Api
  class DocumentsController < Api::BaseController
    before_action :authenticate_request

    def index
      render json: Document.all.as_json
    end
  end
end
