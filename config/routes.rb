Rails.application.routes.draw do
  namespace :api do
    get 'documents/index'
  end

  devise_for :users, controllers: { omniauth_callbacks: 'callbacks' }

  devise_scope :user do
    delete 'sign_out', to: 'sessions#destroy', as: :destroy_user_session
  end

  root 'documents#index'

  resources :documents
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
